from binary2csvparser.process import *
import unittest
from unittest import TestCase
from unittest.case import skipIf
import time


__author__ = 'mariano'


# This test runs the method convertfilebyblock2()
# that is a remake of the method convertfilebyblock() using
# the python package "construct".
class TestConvertFileByBlock(TestCase):
    # This method executes after each test
    def tearDown(self):
        time.sleep(1)  # sleep time in seconds, between tests

    # Tests the structure sizes used on binary data conversion
    @skipIf(False, "Skipping test_bin_struct_sizes")
    def test_bin_struct_sizes(self):
        print "sizeof(ADC0Data_t) = %s bytes" % ADC0Data_t.sizeof()
        print "sizeof(ADC1Data_t) = %s bytes" % ADC1Data_t.sizeof()#ADC_Data_t
        print "sizeof(ADC_Data_t) = %s bytes" % ADC_Data_t.sizeof()
        print "ADC_BUFFER_MAX_SIZE = %s bytes" % ADC_BUFFER_MAX_SIZE
        print "sizeof(ADCDataBuffer_t) = %s bytes" % ADCDataBuffer_t.sizeof()


    # Tests the method convertfilebyblock2() used to convert binary files to CSV
    @skipIf(False, "Skipping test_convertfilebyblock2")
    def test_convertfilebyblock2(self):
        rawfile_name = "/home/diremar/rawfiles-nana/test_data/0012/0.BIN"
        output_file = '/home/diremar/rawfiles-nana/test_data/0012/result.csv'
        convertfilebyblock2(filename=rawfile_name, outputfile=output_file)


# This is needed to run all tests on the file automatically
if __name__ == '__main__':
    unittest.main()

