import sys, argparse, struct, math, binascii
import codecs
import os
from glob import glob
from construct import Struct,ULInt16,ULInt32, Embed, Array


def decode(data):
    values = struct.unpack('I', data[0:4])
    time = values[0]
    adc1 = values[1]
    lat_deg = math.degrees(latitude)
    adc2 = values[2]
    lon_deg = math.degrees(longitude)


# fmt = "<L3Q4L520s28s"
# attrs, creation, access, write, sizeHigh, sizeLow, reserved0, reserved1, name, alternateName = struct.unpack(fmt, data[0:592])
# name = codecs.utf_16_le_decode(name)[0].strip('\x00')
# alternateName = codecs.utf_16_le_decode(alternateName)[0].strip('\x00')
# print 'results:', name

def twoDigitHex(number):
    return '%02x' % number


def readData(filename):
    f = open(filename, 'rb')
    data = f.read()
    f.close()
    return data


def convert_to_ascii(text):
    return " ".join(str(ord(char)) for char in text)


def convertfile(filename, outputfile):
    f = open(filename, 'rb')
    if outputfile != None:
        writeFileName = outputfile
    else:
        writeFileName = filename[0:len(filename) - 3] + 'CSV'

    print writeFileName
    if outputfile != None:
        w = open(writeFileName, 'a')
    else:
        w = open(writeFileName, 'w')
    byte = f.read(2)
    ret = int("%04X" % struct.unpack("<H", binascii.unhexlify(byte.encode("hex"))), 16)
    block_count = ret
    # print block_count
    i = 0
    row = 0
    time1 = 0
    time2 = 0
    current_count = 0

    while byte:
        ret = "%04X" % struct.unpack("<H", binascii.unhexlify(byte.encode("hex")))
        ret = int(ret, 16)
        if current_count == 0 and ret == block_count:
            current_count = ret
            i = 0
            print block_count
        if i >= 2:

            if (not (ret != block_count and current_count == 0)):
                if time1 == 0:
                    time1 = ret
                elif time2 == 0:
                    time2 = ret * (2 ** 16) + time1
                    # print time1, time2, ret
                    w.write(str(time2) + ',')
                    row = row + 1
                else:
                    w.write(str(ret) + ',')
                    row = row + 1

                if row >= 5:
                    w.write('\n')
                    row = 0
                    time1 = 0
                    time2 = 0
                    current_count = current_count - 1
        # break

        byte = f.read(2)
        i = i + 1

    w.close()
    f.close()


def convert_byte_to_integer(byte):
    # print len(byte)
    ret = "%04X" % struct.unpack("<H", binascii.unhexlify(byte.encode("hex")))
    ret = int(ret, 16)
    return ret


def convert_system_time(a, b, c, d):
    return (d * (2 ** 16) + c) * 1000 + b * (2 ** 16) + a

################################################
# TODO REMOVE THE hackbuffsize PARAM WHEN UNITS
# 01 AND 06 ARE DEPLOYED OK WITH BUFFER SIZE 250
def convertfilebyblock(filename, outputfile=None, hack_buff_size=False):
    f = open(filename, 'rb')
    if outputfile != None:
        writeFileName = outputfile
    else:
        writeFileName = filename[0:len(filename) - 3] + 'CSV'

    print writeFileName
    if outputfile != None:
        w = open(writeFileName, 'a')
    else:
        w = open(writeFileName, 'w')

    data_size = 6 * 2

    block = f.read(512)
    blockNum = 0
    while block:
        #		print 'Block:', blockNum
        data_count = convert_byte_to_integer(block[0:2])
        #		print 'DataCount: ', data_count
        if data_count > 41:
            break

        reference_time_micro = convert_system_time(convert_byte_to_integer(block[4:6]),
                                                   convert_byte_to_integer(block[6:8]),
                                                   convert_byte_to_integer(block[8:10]),
                                                   convert_byte_to_integer(block[10:12]))
        index = 0
        while index < data_count:
            #			print index
            try:
                timestamp = reference_time_micro + 20 * index
                bcg_raw = convert_byte_to_integer(block[(12 + index * data_size):(14 + index * data_size)])
                # print 'first',(12+index*data_size),(14+index*data_size)
                ecg_raw = convert_byte_to_integer(block[(14 + index * data_size):(16 + index * data_size)])
                # print 'second',(14+index*data_size),(16+index*data_size)
                mic_raw = convert_byte_to_integer(block[(16 + index * data_size):(18 + index * data_size)])
                # print 'third',(16+index*data_size),(18+index*data_size)
                temp_raw = convert_byte_to_integer(block[(18 + index * data_size):(20 + index * data_size)])
                # print 'fourth',(18+index*data_size),(20+index*data_size)
                stage1_raw = convert_byte_to_integer(block[(20 + index * data_size):(22 + index * data_size)])
                tosa = convert_byte_to_integer(block[(22 + index * data_size):(24 + index * data_size)])
            except:
                print 'ERROR'
                timestamp = 0
                bcg_raw = 0
                ecg_raw = 0
                mic_raw = 0
                temp_raw = 0
                stage1_raw = 0
                tosa = 0

            w.write(str(timestamp) + ',' + str(bcg_raw) + ',' + str(ecg_raw) + ',' + str(mic_raw) + ',' + str(
                temp_raw) + ',' + str(stage1_raw) + ',' + str(tosa) + '\n')
            index = index + 1
        block = f.read(512)
        blockNum = blockNum + 1

    print 'Reached 2nd Level. BlockNum: ', blockNum
    if blockNum < 300:
        while blockNum < 300:
            #			print 'Block:', blockNum
            data_count = 41
            reference_time_micro = 0
            index = 0
            while index <= data_count:
                #				print index
                timestamp = 0
                bcg_raw = 0
                ecg_raw = 0
                mic_raw = 0
                temp_raw = 0
                stage1_raw = 0
                tosa = 0

                w.write(str(timestamp) + ',' + str(bcg_raw) + ',' + str(ecg_raw) + ',' + str(mic_raw) + ',' + str(
                    temp_raw) + ',' + str(stage1_raw) + ',' + str(tosa) + '\n')
                index = index + 1

            blockNum = blockNum + 1

    f.close()
    w.close()

##
# convertfilebyblock2
#
# This method reads binary data from a  raw file,
# processes each block and writes the output to
# a CSV file.
#
''' C Structures
/* Structure for ADC0 */
typedef struct ADC0Data {
	uint16_t 		RawData;
	uint16_t 		ECGData;
	uint16_t		MicData;
	uint16_t		TempData;
}ADC0Data_t;


/* Structure for ADC1 */
typedef struct ADC1Data {
	uint16_t		AmbientData;
	uint16_t 		Stage1Data;
}ADC1Data_t;


/* Structure for ADC Data which has all the relevant data for logging. */
typedef struct ADC_Data{
	ADC0Data_t 		ADC0Data;
	ADC1Data_t 		ADC1Data;
	uint16_t		TOSAValue;
}ADC_Data_t;


/* Structure for the ADC buffers */
typedef struct ADCDataBuffer {
	xSemaphoreHandle	BufferSemaphore;
	unsigned long		UTCTime;
	unsigned long 		MilliSeconds;
	ADC_Data_t			ADCData[ADC_BUFFER_MAX_SIZE];
}ADCDataBuffer_t;
'''
# Python equivalent "structures" using construct package
ADC0Data_t = Struct("ADC0Data",
	ULInt16("RawData"),
	ULInt16("ECGData"),
	ULInt16("MicData"),
	ULInt16("TempData"),
)

ADC1Data_t = Struct("ADC1Data",
	ULInt16("AmbientData"),  # Light sensor data
	ULInt16("Stage1Data"),
)

ADC_Data_t = Struct("ADC_Data",
    ADC0Data_t,
    ADC1Data_t,
    ULInt16("TOSAValue"),
)

#############################################
# TODO UNCOMMENT THIS BLOCK WHEN UNITS 01 AND 06
# ARE DEPLOYED OK WITH BUFFER SIZE 250
#
# ADC_BUFFER_MAX_SIZE = 250
# ADCDataBuffer_t = Struct("ADCDataBuffer",
# 	ULInt32("xSemaphoreHandle"),
#     ULInt32("UTCTime"),
#     ULInt32("MilliSeconds"),
#     Array(ADC_BUFFER_MAX_SIZE, ADC_Data_t),
# )
#############################################

################################################
# TODO REMOVE THE hackbuffsize PARAM WHEN UNITS
# 01 AND 06 ARE DEPLOYED OK WITH BUFFER SIZE 250
def convertfilebyblock2(filename, outputfile=None, timestamp_offset_ms=0, hack_buff_size=False):

    #############################################
    # TODO REMOVE THIS BLOCK WHEN UNITS 01 AND 06
    # ARE DEPLOYED OK WITH BUFFER SIZE 250
    #
    # This is a workaround requested by Vishal for units 01 and 06 only.
    # These two units were deployed with ADC_BUFFER_MAX_SIZE = 500.
    # This code hacks the buffer size re-defining the structures.
    if hack_buff_size:
        # Overwrites the structure definition
        ADC_BUFFER_MAX_SIZE = 500
        print "\n [convertfilebyblock2]> WRN: Using ADC_BUFFER_MAX_SIZE = 500 on file %s " % filename
    else:
        ADC_BUFFER_MAX_SIZE = 250

    ADCDataBuffer_t = Struct("ADCDataBuffer",
        ULInt32("xSemaphoreHandle"),
        ULInt32("UTCTime"),
        ULInt32("MilliSeconds"),
        Array(ADC_BUFFER_MAX_SIZE, ADC_Data_t),
    )
    #############################################

    # Open the input file in binary mode for reading
    f = open(filename, 'rb')

    # If none output filename is specified, then use input filename .CSV
    if outputfile != None:
        writeFileName = outputfile
    else:
        writeFileName = filename[0:len(filename) - 3] + 'CSV'

    print writeFileName
    # If the file exists append to it
    if outputfile != None:
        w = open(writeFileName, 'a')
    else: # If the file doesn't exist, crete it
        w = open(writeFileName, 'w')

    print "\n [convertfilebyblock2]> Converting file %s > %s" % (filename, writeFileName)

    # Read one block from file
    try:
        block = f.read(ADCDataBuffer_t.sizeof())
        block_num = 0
        RECORD_OFFSET_MILLISECONDS = 20 #ms
        while block: # Process file block by block
            print "\n [convertfilebyblock2]> Processing block %s.." % block_num

            parsed_block = ADCDataBuffer_t.parse(block)
            #print parsed_block

            # Extract the data from the block
            # xSemaphoreHandle is ignored
            # Timestamp: (UTC in seconds * 1000) + milliseconds + 20ms by record
            timestamp = parsed_block.UTCTime*1000 + parsed_block.MilliSeconds
            adc_data = parsed_block.ADC_Data
            adc_data_len = len(adc_data)
            for i in range(0,adc_data_len): # Creates a new row for each element in ADCData[]
                # Prepare the data of the row
                raw_data = adc_data[i].ADC0Data.RawData
                ecg_data = adc_data[i].ADC0Data.ECGData
                mic_data = adc_data[i].ADC0Data.MicData
                temp_data = adc_data[i].ADC0Data.TempData
                ambient_data = adc_data[i].ADC1Data.AmbientData
                stage1_data = adc_data[i].ADC1Data.Stage1Data
                tosa_value = adc_data[i].TOSAValue

                # Write the row to CSV file
                row_txt = str(timestamp) + \
                          ',' + str(raw_data) + \
                          ',' + str(ecg_data) + \
                          ',' + str(mic_data) + \
                          ',' + str(temp_data) + \
                          ',' + str(ambient_data) + \
                          ',' + str(stage1_data) + \
                          ',' + str(tosa_value) + \
                          '\n'
                w.write(row_txt)
                #print "\n [convertfilebyblock2]> block:%s, new row = %s" %(block_num, row_txt)
                timestamp = timestamp + RECORD_OFFSET_MILLISECONDS # Increase timestamp

            # Move to next block
            block_num = block_num + 1
            block = f.read(ADCDataBuffer_t.sizeof())
    except Exception, e:
        print "\n [convertfilebyblock2]> ERROR converting file %s : %s" % (filename, e)

    # Close files
    f.close()
    w.close()


def getnextfilename(currentfilename, fileend):
    fileindex = currentfilename[3:len(currentfilename) - 4]
    fileendindex = fileend[3:len(fileend) - 4]
    if int(fileindex) < int(fileendindex):
        currentfileindex = int(fileindex) + 1
        currentfilename = 'DAT' + '%04d' % currentfileindex + '.BIN'
    else:
        currentfilename = 0
    return currentfilename


def main():
    parser = argparse.ArgumentParser(description="Binary File Parser")
    parser.add_argument('--autorunning', dest='autorunning', required=False)
    parser.add_argument('--dataversion', dest='dataversion', required=False)

    #############################################
    # TODO REMOVE THIS BLOCK WHEN UNITS 01 AND 06
    # ARE DEPLOYED OK WITH BUFFER SIZE 250
    # This is a workaround requested by Vishal for units 01 and 06 only.
    # These two units were deployed with ADC_BUFFER_MAX_SIZE = 500.
    # The flag hackbuffsize makes the parser to modify the structure definition
    # to use ADC_BUFFER_MAX_SIZE = 250
    parser.add_argument('--hackbuffsize', dest='hackbuffsize', required=False)
    #############################################

    args = parser.parse_args()
    autorunning = args.autorunning
    print "autorunning ", autorunning
    dataversion = args.dataversion
    print "data version ", dataversion

    #############################################
    # TODO REMOVE THIS BLOCK WHEN UNITS 01 AND 06
    # ARE DEPLOYED OK WITH BUFFER SIZE 250
    hackbuffsize = args.hackbuffsize
    print "hackbuffsize ", hackbuffsize
    if hackbuffsize == 'true':
        hackbuffsize = True
    else:
        hackbuffsize = False
    #############################################

    current_output_index = 0
    if autorunning:
        outputfile = 'result' + '%03d' % current_output_index + '.csv'
    else:
        outputfile = None

    # Data structure is version 2
    if dataversion == '1':
        convertfile = convertfilebyblock
    else:
        convertfile = convertfilebyblock2

    files_list = glob(os.path.join('*.BIN'))
    fileindex = 0
    for a_file in sorted(files_list):
        filename = a_file
        print filename
        ################################################
        # TODO REMOVE THE hackbuffsize PARAM WHEN UNITS
        # 01 AND 06 ARE DEPLOYED OK WITH BUFFER SIZE 250
        convertfile(filename, outputfile, hack_buff_size=hackbuffsize)
        ################################################
        fileindex = fileindex + 1
        if autorunning and (fileindex % 10 == 0):
            current_output_index = current_output_index + 1
            outputfile = 'result' + '%03d' % current_output_index + '.csv'


if __name__ == '__main__':
    main()
