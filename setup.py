__author__ = 'mariano'
from setuptools import setup, find_packages

setup(name = 'binary2csvparser',
      version = '0.7',
      description = 'Python module that converts raw files to CSV',
      #author='Greg Ward',
      #author_email='gward@python.net',
      #url='https://www.python.org/sigs/distutils-sig/',
      packages = find_packages(),
     )